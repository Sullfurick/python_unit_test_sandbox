from main import sum_with_for_string_array

LIST_STRING = ["a", "ab", "abc"]
LIST_INT = [1, 2, 3]


def test_init():
    assert 1 == 1


def test_sum_with_for_string_array():
    assert sum_with_for_string_array(LIST_STRING) == (6, 2.0)


def test_crane():
    assert sum_with_for_string_array(LIST_INT) == (6, 2.0)
