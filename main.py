list_train = ["orel", "bob", "stephen", "helio", "dust", "thomas"]
list_train2 = ["Chien", "Chat", "Souris"]
list_train3 = ["orel"]


def sum_with_for_string_array(my_list):
    """
    Return total numbers in list + average number of letters in each word using for
    """
    total_letters = 0
    words_in_list = len(my_list)
    for i in range(words_in_list):
        total_letters += len(my_list[i])

    avg_letters = total_letters / words_in_list if (words_in_list > 0) else 0
    return total_letters, avg_letters


def sum_with_while_string_array(my_list):
    """
        Return total numbers in list + average number of letters in each word using while
    """
    total_letters = 0
    words_in_list = len(my_list)
    curseur = 0
    while curseur < words_in_list:
        total_letters += len(my_list[curseur])
        curseur += 1
    avg_letters = total_letters / len(my_list) if (words_in_list > 0) else 0
    return total_letters, avg_letters


def sum_with_map_string_array(my_list):
    """
        Return total numbers in list + average number of letters in each word using map
    """

    def size(word):
        return len(word)

    result = map(size, my_list)
    total_letters = sum(list(result))
    avg_letters = total_letters / len(my_list) if (len(my_list) > 0) else 0
    # if len(my_list) > 0:
    #    avg_letters = total_letters / len(my_list)
    # else:
    #    avg_letters = 0
    return total_letters, avg_letters


def filter_even_words(my_list):
    """
        Filter even ands odds words
    """

    def even_len(word):
        return (len(word) % 2) == 0

    def odds_len(word):
        return (len(word) % 2) == 1

    even_words = list(filter(even_len, my_list))
    odds_words = list(filter(odds_len, my_list))
    total_letters_even_word = sum_with_for_string_array(even_words)
    total_letters_odds_word = sum_with_for_string_array(odds_words)
    print(f"Mots pairs : {even_words}, Total de lettres :{total_letters_even_word}, Mots impairs : {odds_words},"
          f" Total de lettres :{total_letters_odds_word}")


def append_to_list(my_list):
    word_to_append = input("Nouveau mot : ")
    my_list.append(word_to_append)
    return my_list


print("********************* METHODE DE BASE *********************")
print(sum_with_for_string_array(list_train))
print(sum_with_while_string_array(my_list=list_train))
print(sum_with_map_string_array(list_train))
print("***************** METHODE FILTER EVEN/ODDS *****************")
filter_even_words(list_train)
print("***************** METHODE APPEND *****************")
print(append_to_list(list_train3))
