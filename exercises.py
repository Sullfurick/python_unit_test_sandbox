import re

word1 = 5
word2 = "king"
word3 = "horse"
word4 = "STONE"


def find_letter_in_word(word):
    lower_word = word.lower()
    for i in range(len(lower_word)):
        result = True if "e" in lower_word else False
        return result


def find_letter_in_word_one_line(word):
    result = "Caractère '{}' présent : {}, Mot : {}".format("e", "Oui" if "e" in word.lower() else "Non", word)
    return result


def find_custom_letter_in_word(word):
    letter = input("Lettre à chercher :")
    result = f"Caractère {letter} présent : {'Oui' if type(word) == str and letter.lower() in word.lower() else 'Non'}, Mot : {word}"
    return result


def find_custom_letter_panda(word):
    letter = input("Lettre à chercher :")
    return word.str.contains(letter, case=False, regex=False)


print(find_letter_in_word(word2))
print(find_letter_in_word(word3))
print(find_letter_in_word(word4))
print("****************************************************")
print(find_letter_in_word_one_line(word2))
print(find_letter_in_word_one_line(word3))
print(find_letter_in_word_one_line(word4))
print("****************************************************")
print(find_custom_letter_in_word(word1))
print(find_custom_letter_in_word(word2))
print(find_custom_letter_in_word(word3))
print(find_custom_letter_in_word(word4))

# print(find_custom_letter_panda(word1))
print(find_custom_letter_panda(word2))
print(find_custom_letter_panda(word3))
print(find_custom_letter_panda(word4))
