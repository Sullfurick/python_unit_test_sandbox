my_string1 = 120
my_string2 = "boblebrico"


def insert_char_in_string(my_string):
    string_to_modify = str(my_string)
    return '*'.join(string_to_modify)


def reverse_a_string(my_string):
    string_to_reverse = str(my_string)
    return ''.join(reversed(string_to_reverse))


print(insert_char_in_string(my_string1))
print(insert_char_in_string(my_string2))
print("*********************************")
print(reverse_a_string(my_string1))
print(reverse_a_string(my_string2))
