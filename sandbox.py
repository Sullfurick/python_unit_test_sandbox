company = ["apple", "amazon", "ubisoft", "milka", "renault"]
stock = [[15, 2, 3, 4, 5], [1, 17, 3, 4, 5], [1, 2, 3, 4, 5]]

avg_list = []
avg_dict = {}
for i in range(len(company)):
    sum_current_company = 0
    for j in range(len(stock)):
        # print(f"company : {company[i]} === {stock[j][i]}")
        sum_current_company += stock[j][i]  # somme de la compagnie actuelle = prix du jour de la compagnie
    avg = sum_current_company / len(stock)
    avg_list.append(avg)
    avg_dict[company[i]] = avg

print(avg_list)
print(avg_dict)
