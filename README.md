# PYTHON SANDBOX : TESTS
```python
# Clone project
git clone https://gitlab.com/Sullfurick/python_unit_test_sandbox
cd /python_unit_test_sandbox
pip install pytest

# Run main file
python3 main.py
# Run tests file
pytest ./test_methods_string_array.py

```

